

from tkinter import *
from Board import *


root = Tk()

button_size = 83

black = PhotoImage(file="BlackField.png")
black = black.zoom(2)
white = PhotoImage(file="WhiteField.png")
white = white.zoom(2)
none = PhotoImage(file="Field.png")
none = none.zoom(2)
but1 = PhotoImage(file="But1.png")
but1 = but1.subsample(2)
but2 = PhotoImage(file="But2.png")
but2 = but2.subsample(2)
but3 = PhotoImage(file="But3.png")
but3 = but3.subsample(2)
but4 = PhotoImage(file="But4.png")
but4 = but4.subsample(2)
but5 = PhotoImage(file="But5.png")
but5 = but5.subsample(2)
but6 = PhotoImage(file="But6.png")
but6 = but6.subsample(2)

yellow_white = PhotoImage(file="WhiteFieldActive.png")
yellow_white = yellow_white.zoom(2)

yellow_black = PhotoImage(file="BlackFieldActive.png")
yellow_black = yellow_black.zoom(2)

root.geometry('1000x1000')


class Main:
    def __init__(self):
        self.buttons = [[0 for i in range(j)] for j in range(5, 10)] + [[0 for i in range(13 - j)] for j in range(5, 9)]
        self.selected = []
        self.pushed = []
        self.l = Label()
        self.counter1 = Label()
        self.counter2 = Label()
        self.board=Board()                                                                  #   self.bind('<Enter>', self.move_restart())

    def move_restart(self):
        self.selected.clear()
        self.update()
                                                                                # выделение шарика желтым цветом

    def make_it_yellow(self):
        for ball in self.selected:
            n = self.board.num[ball[0]].index(ball[1])
            m = self.board.draw_array[ball[0]][n]
            if m == 1 and self.board.white_is_move == 1:
                self.update()
            elif m == -1 and self.board.white_is_move == -1:
                self.update()
            else:
                self.move_restart()
                                                                           # обработка нажатий на клетки

    def tap(self, X, Y):
        var = self.selected
        deleg = self.make_it_yellow
        deleg1 = self.update

        def wrapper(v=var, x=X, y=Y, delegat = deleg, delegat1 = deleg1):
            if len(var) >= 3:
                self.move_restart()
            if [x, y] not in var:
                var.append([x, y])
                delegat()
                self.make_it_yellow()
            elif [x, y] in var:
                var.remove([x,y])
                delegat()
                delegat1()

        return wrapper                                             # проверить цвет нужного нам шарика

    def my_color(self, ball, x, y):
        if 0 <= ball[0] <= 8:
            if ball[1] in self.board.num[ball[0]]:
                n = self.board.num[ball[0]].index(ball[1])
                return self.board.draw_array[ball[0]][n]
            else:
                return 'out'
        else:
            return 'out'

    def simple_move(self,x ,y ):  # заменяю выделенные клетки на пустые
        for ball in self.selected:
            n = self.board.num[ball[0]].index(ball[1])
            self.board.draw_array[ball[0]][n] = 0
        for ball in self.selected:  # меняю цвета с учетом сдвига на цвет хода
            m = self.board.num[ball[0] + x].index(ball[1] + y)
            self.board.draw_array[ball[0] + x][m] = self.board.white_is_move
        self.board.white_is_move = - self.board.white_is_move
        self.move_restart()
        self.board.count()
        if self.board.white == 8:
            win=Label(text='BLACKE WIN')
            win.pack()
            self.pushed.clear()  # тут мы двигаем шарики соперника
        elif self.board.black == 8:
            win=Label(text='WHITE WIN')
            win.pack()
            self.pushed.clear()  # тут мы двигаем шарики соперника



    def move(self, x, y):
        print('after i will push')
        for ball in self.pushed:
            n = self.board.num[ball[0]].index(ball[1])
            self.board.draw_array[ball[0]][n] = 0
        for ball in self.pushed:
            m = self.board.num[ball[0]].index(ball[1])
            self.board.draw_array[ball[0]][m] = - self.board.white_is_move

        self.update()
        print(str(self.pushed) + '.. pushed to')
        self.pushed.clear()

    def game_record(self):

        self.board.gamefile.write(str(self.board.draw_array))

    def stop_record(self):
        self.board.gamefile.close()

    def next_cell(self, x, y):  # проверка следующих клеток после выделенных
        if len(self.selected) == 1:
            for ball in self.selected:
                nball = [ball[0] + x, ball[1] + y]
                if self.my_color(nball, x, y) == 0:
                    self.simple_move(x, y)
                else:
                    print('wrong move')
                    self.move_restart()
        elif len(self.selected) == 2:
            new = []
            for ball in self.selected:
                new.append([ball[0] + x, ball[1] + y])
            for ball in new:
                if ball not in self.selected:
                    if self.my_color(ball, x, y) == 0:
                        self.simple_move(x, y)
                        break
                    elif self.my_color(ball, x, y) == -self.board.white_is_move:
                        nball = [ball[0] + x, ball[1] + y]
                        self.pushed.append(nball)
                        if self.my_color(nball, x, y) == 0:
                            self.move(x, y)
                            self.simple_move(x, y)
                        elif self.my_color(nball, x, y) == 'out':
                            self.simple_move(x, y)
                            self.pushed.clear()
                        else:
                            self.move_restart()
                    else:
                        self.move_restart()
        elif len(self.selected) == 3:
            new = []
            for ball in self.selected:
                new.append([ball[0] + x, ball[1] + y])

            #print(str(new) + '-new')
            for ball in new:
                if ball not in self.selected:
                    if self.my_color(ball, x, y) == 0:
                        print('3 balls and the next cell is empty')
                        self.simple_move(x, y)

                        break
                    elif self.my_color(ball, x, y) == -self.board.white_is_move:
                        n2ball = [ball[0] + x, ball[1] + y]
                        self.pushed.append(n2ball)
                        if self.my_color(n2ball, x, y) == 0:
                            self.move(x, y)
                            self.simple_move(x, y)
                            print('3 balls and the next cell is other color after empty')
                            break
                        elif self.my_color(n2ball, x, y) == 'out':
                            self.simple_move(x,y)
                            self.pushed.clear()
                        elif self.my_color(n2ball, x, y) == -self.board.white_is_move:
                            n3ball = [n2ball[0] + x, n2ball[1] + y]
                            self.pushed.append(n3ball)

                            if self.my_color(n3ball, x, y) == 0:
                                self.move(x, y)
                                self.simple_move(x, y)
                                print('3 balls and the 2 next cell - other color, after is empty')
                                break
                            elif self.my_color(n3ball, x, y) == 'out':
                                self.simple_move(x,y)
                                self.pushed.clear()
                            else:
                                self.move_restart()
                        else:
                            self.move_restart()
                    else:
                        self.move_restart()

                        # нужно проверить перед move, что цвет массива совпадает c white_is_move (что ваш ход)

    def check_color(self, x, y):

        ball = self.selected[0]
        if self.my_color(ball, x, y) == self.board.white_is_move:
            self.next_cell(x, y)
        else:
            print('not you turn!')
            self.move_restart()

    def check_color_and_next_not_other_colored(self, x, y):
        ball = self.selected[0]
        if self.my_color(ball, x, y) == self.board.white_is_move:
            for ball in self.selected:
                ball = [ball[0]+x, ball[1]+y]
                if self.my_color(ball, x, y) !=0:
                    print('wrong')
                    self.move_restart()
                    break
            self.next_cell(x, y)
        else:
            print('not you turn!')
            self.move_restart()

    def go_to(self, X, Y):

        var = self.selected
        var2 = self.check_color  # куча проверок корректности хода
        var3 = self.check_color_and_next_not_other_colored
        def wrapper(v=var, x=X, y=Y, check_color=var2, check_color_and_next_not_other_colored=var3):
            colors = []
            for ball in var:  # все ли выделенные шарики одного цвета?
                n = self.board.num[ball[0]].index(ball[1])
                m = self.board.draw_array[ball[0]][n]
                colors.append(m)
                if 0 <= ball[0] + x <= 8:
                    a = self.board.num[ball[0] + x]
                    if ball[1] + y in a:  # рядом ли выбранные шарики и не противоречат ли направлению
                        if len(var) == 1:
                            check_color(x, y)
                        elif len(var) == 2:
                            if var[0][0] == var[1][0] and abs(var[0][1] - var[1][1]) == 1 and x == 0:
                                check_color(x, y)
                            elif abs(var[0][0] - var[1][0]) == 1 and var[0][1] == var[1][1] and y == 0:
                                check_color(x, y)
                            elif abs(var[0][0] - var[1][0]) == 1 and abs(var[0][1] - var[1][1]) == 1 and abs(x) == 1 and abs(y) == 1:
                                check_color(x, y)
                            elif var[0][0] == var[1][0] and abs(var[0][1] - var[1][1]) == 1 and x != 0:
                                check_color_and_next_not_other_colored(x, y)
                            elif abs(var[0][0] - var[1][0]) == 1 and var[0][1] == var[1][1] and y != 0:
                                check_color_and_next_not_other_colored(x, y)
                            elif abs(var[0][0] - var[1][0]) == 1 and abs(var[0][1] - var[1][1]) == 1 and  (abs(x) == 1 and abs(y)) != 1:
                                check_color_and_next_not_other_colored(x, y)
                            else:
                                print('you cannot do this move')
                        elif len(var) == 3:
                            xs = [var[0][0], var[1][0], var[2][0]]
                            ys = [var[0][1], var[1][1], var[2][1]]
                            if max(xs) == min(xs) and max(ys) - min(ys) == 2 and x == 0:
                                check_color(x, y)
                            elif max(xs) - min(xs) == 2 and max(ys) == min(ys) and y == 0:
                                check_color(x, y)
                            elif max(xs) - min(xs) == 2 and max(ys) - min(ys) == 2 and abs(x) == 1 and abs(y) == 1:
                                check_color(x, y)
                            elif max(xs) == min(xs) and max(ys) - min(ys) == 2 and x != 0:
                                check_color_and_next_not_other_colored(x, y)
                            elif max(xs) - min(xs) == 2 and max(ys) == min(ys) and y != 0:
                                check_color_and_next_not_other_colored(x, y)
                            elif max(xs) - min(xs) == 2 and max(ys) - min(ys) == 2 and not (abs(x) == 1 and abs(y) == 1):
                                check_color_and_next_not_other_colored(x, y)
                            else:
                                print('you cannot do this move')
                    else:  # не вылезаю ли за границы поля своим шариком
                        print('bad! try again')
                        self.move_restart()
                        break
                else:
                    print('bad! try again')
                    self.move_restart()
                    break
            if 0 in colors:
                print('empty cell was selected')
                self.move_restart()
            if 1 in colors and -1 in colors:
                print('you should move only your balls')
                self.move_restart()
        return wrapper

    def draw_buttons(self):
        for i in range(5):
            for j in range(9 - i):
                b = Button(root, activebackground='blue', relief = FLAT, command=self.tap(4 + i, i + j))
                b.place(x=button_size + i * button_size / 2 + button_size * j,
                        y=button_size + i * button_size + button_size * 5)
                self.buttons[i + 4][j] = b
        for i in range(4):
            for j in range(8 - i):
                b = Button(root, activebackground='blue', relief = FLAT, command=self.tap(3 - i, j))
                b.place(x=1.5 * button_size + i * button_size / 2 + button_size * j,
                        y=-i * button_size + button_size * 5)
                self.buttons[3 - i][j] = b
        self.update()

    def new_game(self):
        self.board = Board()
        self.board.update_draw_array()
        m.draw_buttons()

        self.l.config(text="White's turn")
        self.l.pack()
        self.counter1.config(text='white: ' + str(self.board.white))
        self.counter2.config(text='black: ' + str(self.board.black))
        self.counter1.pack()
        self.counter2.pack()
                                                                          # графика

    def update(self):
      #  print('update gu')
        for i in range(len(self.buttons)):
            for j in range(len(self.buttons[i])):
                if self.board.draw_array[i][j] == 1:
                    self.buttons[i][j].config(image=white)
                elif self.board.draw_array[i][j] == -1:
                    self.buttons[i][j].config(image=black)
                else:
                    self.buttons[i][j].config(image=none)
        if self.board.white_is_move == 1:
            self.l.config(text="White's turn")
        else:
            self.l.config(text="Black's turn")
        for ball in self.selected:
            n = self.board.num[ball[0]].index(ball[1])
            m = self.board.draw_array[ball[0]][n]
            if m == 1 and self.board.white_is_move == 1:
                self.buttons[ball[0]][n].config(image=yellow_white)
            elif m == -1 and self.board.white_is_move == -1:
                n = self.board.num[ball[0]].index(ball[1])
                self.buttons[ball[0]][n].config(image=yellow_black)
        self.l.pack()
        self.counter1.config(text='white: ' + str(self.board.white))
        self.counter2.config(text='black: ' + str(self.board.black))
        self.counter1.pack()
        self.counter2.pack()
        with open("game.txt") as file:
            for line in file:
                print(line)


    def test(self):
        #print('test')
        #self.board.draw_array = [[1,1,1,1,1], [1,1,-1,-1,-1,0],[-1,-1,-1,-1,0,0,0],[-1,-1,-1,0,0,0,0,0],[-1,0,0,0,0,0,0,0,0]] + [[0 for i in range(13 - j)] for j in range(5, 9)]
        pass

if __name__ == "__main__":
    m = Main()
    start = Button(text='start', command=m.new_game)
    start.pack()

    # выбор направления
    up1 = Button(text='-', image=but1, relief=FLAT, command=m.go_to(-1, -1))
    up1.place(x=870, y=700)
    down1 = Button(text='-', image=but5,relief=FLAT, command=m.go_to(1, 0))
    down1.place(x=870, y=800)
    left = Button(text='-', image=but4, relief=FLAT, command=m.go_to(0, -1))
    left.place(x=850, y=750)
    right = Button(text='-', image=but3,relief=FLAT, command=m.go_to(0, 1))
    right.place(x=940, y=750)
    up2 = Button(text='-', image=but2, relief=FLAT, command=m.go_to(-1, 0))
    up2.place(x=920, y=700)
    down2 = Button(text='-', image=but6, relief=FLAT, command=m.go_to(1, 1))
    down2.place(x=920, y=800)
    useless = Button(command=m.move_restart)
    useless.place(x=900, y=750)
    end_record = Button(text='stop recording', command=m.stop_record())
    end_record.pack()
    test = Button(text='test', command=m.test())
    test.pack()
root.mainloop()